# Configure the GitLab Provider
provider "gitlab" {}

# Add a project owned by the user
resource "gitlab_project" "sample_project" {
    name = "terraform_gitlab"
    visibility_level = "public"
}


# Add a deploy key to the project
resource "gitlab_deploy_key" "sample_deploy_key" {
    project = "${gitlab_project.sample_project.id}"
    title = "terraform"
    key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILd4eXvlkWIIu6HtYdbuFrzgkVPexn40nAwKbpizr6cg vlad@iMac.local"
    can_push = true
}


